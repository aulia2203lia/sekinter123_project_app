import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class AirPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: 412,
        height: 1200,
        decoration: BoxDecoration(
          color: const Color(0xFFD7E0E2),
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(10),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 40.0, 16.0, 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(
                            context); // Navigate back to the previous screen
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Fakta Menarik Air',
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w500,
                            fontSize: 28,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(width: 48),
                  ],
                ),
              ),
              SizedBox(height: 16),
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: 350,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.25),
                        blurRadius: 4,
                        offset: Offset(0, 4),
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        width: 325,
                        height: 100,
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Tahukah kamu? Bahwa air adalah senyawa yang penting bagi semua makhluk di Bumi. Rumus kimianya adalah H₂O, yang setiap molekulnya mengandung satu oksigen dan dua atom hidrogen yang dihubungkan oleh ikatan kovalen. Air menutupi hampir 71% permukaan Bumi.',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Container(
                        width: 325,
                        height: 100,
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Manfaat air yang segar dan bersih antara lain : \na. Menyehatkan Kesehatan, \nb. Menurunkan risiko penyakit kronis, \nc. Memperpanjang usia, \nd. Meningkatkan stamina dan fokus, \ne. Memperbaiki mood',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Container(
                        width: 325,
                        height: 120,
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'air terdiri atas atom-atom. Air tersusun atas oksigen (O2) dan hidrogen (H). air menunjukkan sifat kimia dan fisik yang sangat kompleks. Misalnya, titik lelehnya, 0 derajat C (32 derajat F), dan titik didihnya, 100 derajat C (212 derajat F), jauh lebih tinggi daripada yang diharapkan jika dibandingkan dengan senyawa analog, seperti hidrogen sulfida dan amonia.',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Container(
                        width: 325,
                        height: 160,
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Air yang kita gunakan sehari-hari seperti minum, memasak, mandi dan lainnya harus dalam keadaan bersih sehingga kita dapat terhindar dari penyakit yang disebabkan karena kualitas air buruk. Dengan menggunakan air bersih kita dapat terhindar dari penyakit seperti diare, kolera, disentri, tipes, cacingan, penyakit kulit hingga keracunan. Untuk itu wajib bagi seluruh anggota keluarga untuk menggunakan air bersih setiap hari dan menjaga kualitas air agar tetap bersih di lingkungannya.',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      SizedBox(height: 5),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 50),
              //tempat kolom zat zat

              Align(
                alignment: Alignment.center,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 348,
                        height: 600,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: Text(
                                  'The highest measurement value',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              SizedBox(height: 10),
                              Column(
                                children: [
                                  Image.asset('images/standarair.png'),
                                  SizedBox(height: 2),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'images/jenis.png',
                                        width: 300,
                                        height: 300,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fakta Menarik Air',
      theme: ThemeData(
        // Ganti dengan warna pilihan Anda
        primarySwatch: Colors.blue,
      ),
      home: AirPage(),
    );
  }
}
