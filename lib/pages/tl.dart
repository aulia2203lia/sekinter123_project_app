import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class LaporPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: 412,
        height: 1200,
        decoration: BoxDecoration(
          color: const Color(0xFFD7E0E2),
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(10),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 40.0, 16.0, 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(
                            context); // Navigate back to the previous screen
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Tindak Lapor',
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w500,
                            fontSize: 28,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(width: 48),
                  ],
                ),
              ),
              SizedBox(height: 16),
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: 350,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.25),
                        blurRadius: 4,
                        offset: Offset(0, 4),
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        width: 325,
                        height: 105,
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Undang-Undang Nomor 5 Tahun 1960 tentang Peraturan Dasar Pokok-Pokok Agraria (“UUPA”) mendefinisikan hak guna air sebagai hak memperoleh air untuk keperluan tertentu dan/atau mengalirkan air itu di atas orang lain.[1] Hak guna air merupakan salah satu bentuk hak atas air dan ruang angkasa.',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Container(
                        width: 325,
                        height: 390,
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Ketentuan Pidana Para pihak yang melanggar ketentuan mengenai pemanfaatan air tanah dijatuhi hukuman pidana dalam Pasal 15 ayat (1) UU Pengairan. Pasal tersebut berbunyi: Diancam dengan hukuman penjara selama-lamanya 2 (dua) tahun dan atau denda setinggi-tingginya Rp. 5.000.000,- (lima juta rupiah): \n\n\t a. Barang siapa dengan sengaja melakukan pengusahaan air dan atau sumber-sumber air yang tidak berdasarkan perencanaan dan perencanaan teknis tata pengaturan air dan tata pengairan serta pembangunan pengairan sebagaimana tersebut dalam Pasal 8 ayat (1) Undang-undang ini; \n\t b. Barang siapa dengan sengaja melakukan pengusahaan air dan atau sumber-sumber air tanpa izin dari Pemerintah sebagaimana tersebut dalam Pasal 11 ayat (2) Undang-undang ini; \n\t c. Barang siapa yang sudah memperoleh izin dari Pemerintah untuk pengusahaan air dan atau sumber-sumber air sebagaimana tersebut dalam Pasal 11 ayat (2) Undang-undang ini, tetapi dengan sengaja tidak melakukan dan atau sengaja tidak ikut membantu dalam usaha-usaha menyelamatkan tanah, air, sumber-sumber air dan bangunan-bangunan pengairan sebagaimana tersebut dalam Pasal 13 ayat (1) huruf a, b, c, dan d Undang-undang ini.',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Container(
                        width: 325,
                        height: 100,
                        margin: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          color: const Color(0xFFD9D9D9),
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Pidana tertuang dari dalam BAB XIV, dari Pasal 68 sampai Pasal 74. Pidana berlaku untuk Setiap orang yang dengan sengaja melakukan kegiatan dan mengakibatkan kerusakan sumber air dan prasarananya atau pencemaran air, serta melakukan kegiatan yang mengakibatkan terjadinya daya rusak air.',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      SizedBox(height: 12),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: 348,
                  height: 220,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 325,
                          height: 80,
                          margin: EdgeInsets.only(bottom: 16),
                          decoration: BoxDecoration(
                            color: const Color(0xFFD9D9D9),
                            border: Border.all(color: Colors.black, width: 0.6),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Daerah anda sumber daya air bersih kurang merata? tidak bersih? kekurangan air bersih? ada seseorang atau kelompok tertentu melakukan pencemaran air? dan lainnya?',
                              style: TextStyle(
                                fontFamily: 'Inter',
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        ),
                        Text(
                          'Lapor ke sini aja! cukup mudah, dengan klik nomor di bawah, lalu konsultasikan masalah terkait air bersih atau pencemaran air. kemudian admin akan membantu permasalahan anda.',
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 5),
                        GestureDetector(
                          onTap: () {
                            // Perform action when the phone number is tapped
                          },
                          child: Text(
                            '0852754285184',
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tindak Lapor',
      theme: ThemeData(
        // Ganti dengan warna pilihan Anda
        primarySwatch: Colors.blue,
      ),
      home: LaporPage(),
    );
  }
}
