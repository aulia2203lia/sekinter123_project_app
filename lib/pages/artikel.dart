import 'package:flutter/material.dart';

void main() {
  runApp(const ArtikelPage());
}

class ArtikelPage extends StatelessWidget {
  const ArtikelPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      home: Scaffold(
        body: ListView(
          children: const [
            Artikel(),
          ],
        ),
      ),
    );
  }
}

class Artikel extends StatelessWidget {
  const Artikel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 410,
          height: 900,
          clipBehavior: Clip.antiAlias,
          decoration: ShapeDecoration(
            color: const Color(0xFFD7E0E2),
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 0.50),
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                left: 0,
                top: 0,
                child: Container(
                  width: 410,
                  height: 232,
                  decoration: ShapeDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/berita3.png'),
                      fit: BoxFit.fill,
                    ),
                    shape: RoundedRectangleBorder(
                      side: BorderSide(width: 0.2),
                      borderRadius: BorderRadius.circular(1),
                    ),
                    shadows: [
                      BoxShadow(
                        color: const Color(0x3F000000),
                        offset: const Offset(0, 2),
                        spreadRadius: 0,
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 70,
                top: 256,
                child: SizedBox(
                  width: 287,
                  height: 40,
                  child: Text(
                    'Sampah di sungai kian memburuk! Waspada penyakit menghantui anda',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 310,
                top: 305,
                child: SizedBox(
                  width: 88,
                  height: 17,
                  child: Text(
                    '4 Agustus 2020',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 5,
                top: 305,
                child: SizedBox(
                  width: 123,
                  height: 17,
                  child: Text(
                    'Oleh Sebastian Riko',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 15,
                top: 330,
                child: SizedBox(
                  width: 380,
                  height: 452,
                  child: SingleChildScrollView(
                    child: Text(
                      'Lorem ipsum dolor sit amet consectetur. Ut enim eget velit in. Hendrerit porttitor bibendum elit ac risus eu cras velit. Semper purus placerat iaculis nisi tempus. Gravida adipiscing vitae risus urna egestas tortor lorem. Id nulla facilisis dictum velit et faucibus tempor at eleifend. Ornare risus lorem consectetur diam orci. Lacus vitae vestibulum ultrices amet massa eu aliquam. Ac tellus et tempus pulvinar a non id odio. Neque iaculis lectus et nisi. Vel id at enim nunc turpis donec fermentum. Et sit sit aliquet massa malesuada. Risus magna risus nibh fermentum orci nullam mauris risus egestas. Iaculis at augue ac cras morbi rhoncus turpis orci. Convallis mattis arcu eget volutpat ut elit malesuada velit justo.\n\nArcu aliquet sed pulvinar non fermentum vel tristique enim. Diam vitae eget ut turpis at convallis arcu ultrices. Nunc risus potenti ut tortor hac mauris. Bibendum viverra massa scelerisque magna. Aliquam mauris posuere massa dui turpis nibh dolor.\n\nTellus at orci tempus non sem integer pretium tellus. Duis lorem nunc gravida lorem. A massa aliquam pharetra posuere sodales. In lobortis in ullamcorper interdum. Commodo vulputate velit amet diam sit nisl vehicula phasellus. Ullamcorper lacus gravida amet amet at sem sapien odio. Arcu convallis id rutrum suspendisse faucibus. Tellus mauris a id lectus quam scelerisque tortor dictumst ullamcorper. Tincidunt eget eget pulvinar ac.\n\nTristique libero quam in nunc pulvinar in semper. Sagittis libero viverra quis dui at egestas aliquam. Adipiscing at nulla at enim et facilisi non. Urna sit hac purus pretium. Ultricies bibendum nunc nibh turpis vel purus consequat. Quis at ut magna amet risus lacus facilisis. Maecenas quam sed et nunc non commodo. Elit nunc proin purus faucibus eget. A augue odio bibendum sodales aliquam eget. Eget porttitor eget odio sollicitudin pellentesque. Tristique tristique sed est amet vehicula aliquam pharetra libero. Amet eget eget egestas amet mauris. Nullam arcu feugiat sed tortor in tincidunt ultrices eget libero.\n',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                        fontFamily: 'Inter',
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 316,
                top: 212,
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: ShapeDecoration(
                    color: const Color(0xFFD9D9D9),
                    shape: const CircleBorder(side: BorderSide(width: 0.50)),
                  ),
                ),
              ),
              Positioned(
                left: 323,
                top: 220,
                child: Container(
                  width: 11,
                  height: 11,
                  child: Icon(
                    Icons.share,
                    color: Colors.black,
                  ),
                ),
              ),
              Positioned(
                top: 10,
                left: 10,
                child: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
