import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/material.dart';

class MapsPage extends StatelessWidget {
  const MapsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lokasi Saat Ini'),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'images/lokasi.png',
            fit: BoxFit.cover,
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                width: 215, // Atur lebar kolom pencarian
                height: 40, // Atur tinggi kolom pencarian
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(50.0),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 4.0,
                ),
                child: Row(
                  children: [
                    Icon(
                      Icons.edit_location,
                      color: Colors.white,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Kaliurang, Yogyakarta',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Tambahkan konten lain di bawah gambar lokasi jika diperlukan
        ],
      ),
    );
  }
}
