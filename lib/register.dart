import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:login1_app_flutter/home.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValController = TextEditingController();

  bool errorValidator = false;

  Future signUp() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordValController.text,
      );
      await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SEKINTER SIGN UP"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.00),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email",
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tuliskan Email anda",
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Password",
            ),
            SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: passwordController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tuliskan Password anda",
                errorText: errorValidator ? "Password Tidak Sama!" : null,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Validasi Password",
            ),
            SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: passwordValController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tulis ulang Password anda",
                errorText: errorValidator ? "Password Tidak Sama!" : null,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              width: double.infinity,
              height: 50.0,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    passwordController.text == passwordValController.text
                        ? errorValidator = false
                        : errorValidator = true;
                  });
                  if (errorValidator) {
                    print("Error");
                  } else {
                    signUp();
                    Navigator.pop(context);
                  }
                },
                child: Text("Daftar"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
