import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:login1_app_flutter/home.dart';
import 'package:login1_app_flutter/register.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  Future signIn() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        padding: const EdgeInsets.all(0),
        color: Color(0xFF99D0EF),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Image.asset(
                'images/login.png',
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: 30.0,
            ),

            //EMAIL
            Center(
              child: SizedBox(
                height: 35.0,
                width: 300.0,
                child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    hintText: "Email",
                    prefixIcon: Image.asset("images/email.png"),
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 20.0,
            ),

            //PASSWORD
            Center(
              child: SizedBox(
                height: 35.0,
                width: 300.0,
                child: TextField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    hintText: "Password",
                    prefixIcon: Image.asset("images/password.png"),
                    suffixIcon: Image.asset("images/forgot.png"),
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 5.0,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(
                    right:
                        55.0), // tambahkan padding ke sisi kanan text "Forgot password?"
                child: Text("Forgot password?"),
              ),
            ),

            SizedBox(
              height: 20.0,
            ),

            //LOGIN
            Center(
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Color(
                      0xFF0094FF)), // mengubah warna latar belakang tombol
                  foregroundColor: MaterialStateProperty.all<Color>(
                      Colors.white), // mengubah warna teks tombol
                  textStyle: MaterialStateProperty.all<TextStyle>(
                      TextStyle(fontSize: 18)), // mengubah ukuran teks tombol
                  minimumSize: MaterialStateProperty.all<Size>(
                      Size(300, 35)), // mengubah ukuran tombol menjadi 120 x 40
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: Colors.black)),
                  ), // mengubah bentuk tombol
                ),
                onPressed: signIn,
                child: Text("Login"),
              ),
            ),

            SizedBox(
              height: 1,
            ),

            Center(
              child: Text("or"),
            ),

            //SIGNUP
            SizedBox(
              height: 1,
            ),
            Center(
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Color(
                      0xFFF6F6F6)), // mengubah warna latar belakang tombol
                  foregroundColor: MaterialStateProperty.all<Color>(
                      Color.fromARGB(
                          255, 0, 0, 0)), // mengubah warna teks tombol
                  textStyle: MaterialStateProperty.all<TextStyle>(
                      TextStyle(fontSize: 17)), // mengubah ukuran teks tombol
                  minimumSize: MaterialStateProperty.all<Size>(
                      Size(300, 35)), // mengubah ukuran tombol menjadi 120 x 40
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: Colors.black)),
                  ), // mengubah bentuk tombol
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterPage(),
                    ),
                  );
                },
                child: Text("Sign Up"),
              ),
            ),

            SizedBox(
              height: 50.0,
            ),

            Center(
              child: Text("Login with"),
            ),
            SizedBox(
              height: 10.0,
            ),
            //TOMBOL GOOGLE
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 95,
                  height: 40,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.white), // mengubah warna latar belakang tombol
                      foregroundColor: MaterialStateProperty.all<Color>(
                          Colors.black), // mengubah warna teks tombol
                      textStyle: MaterialStateProperty.all<TextStyle>(TextStyle(
                          fontSize: 12)), // mengubah ukuran teks tombol
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                            side: BorderSide(color: Colors.black)),
                      ), // mengubah bentuk tombol
                    ),
                    onPressed: () {
                      signInWithGoogle();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.asset(
                          "images/google.png",
                          width: 18,
                          height: 18,
                        ),
                        Text("Google"),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 3),
                //Twitter
                SizedBox(
                  width: 95,
                  height: 40,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.white), // mengubah warna latar belakang tombol
                      foregroundColor: MaterialStateProperty.all<Color>(
                          Colors.black), // mengubah warna teks tombol
                      textStyle: MaterialStateProperty.all<TextStyle>(TextStyle(
                          fontSize: 12)), // mengubah ukuran teks tombol
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                            side: BorderSide(color: Colors.black)),
                      ), // mengubah bentuk tombol
                    ),
                    onPressed: () {},
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.asset(
                          "images/twitter.png",
                          width: 18,
                          height: 18,
                        ),
                        Text("Twitter"),
                      ],
                    ),
                  ),
                ),
              ],
            ),

            SizedBox(
              height: 5.0,
            ),

            //FACEBOOK
            Center(
              child: SizedBox(
                width: 95,
                height: 40,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        Colors.white), // mengubah warna latar belakang tombol
                    foregroundColor: MaterialStateProperty.all<Color>(
                        Colors.black), // mengubah warna teks tombol
                    textStyle: MaterialStateProperty.all<TextStyle>(
                        TextStyle(fontSize: 11)), // mengubah ukuran teks tombol
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                          side: BorderSide(color: Colors.black)),
                    ), // mengubah bentuk tombol
                  ),
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset(
                        "images/fbb.png",
                        width: 15,
                        height: 15,
                      ),
                      Text("Facebook"),
                    ],
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}
